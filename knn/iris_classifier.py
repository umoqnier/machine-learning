# Authors = Diego A. Barriga (@umoqnier) y Sandra A. Real (@sangreal02 )
# Repo = https://gitlab.com/umoqnier/machine-learning.git
# This code is base on the proposed at https://kevinzakka.github.io/2016/07/13/k-nearest-neighbor/
# Credits = Kevin Zakka (@kevinzakka)

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from collections import Counter


def plot_data(x, y, feature):
    setosa = plt.scatter(x.iloc[:52], y.iloc[:52], marker='.', color='r')
    versicolor = plt.scatter(x.iloc[52:102], y.iloc[52:102], marker='2', color='b')
    virginica = plt.scatter(x.iloc[102:], y.iloc[102:], marker='1', color='g')
    plt.legend((setosa, versicolor, virginica),
               ('Setosa', 'Versicolor', "Virginica"),
               scatterpoints=3,
               loc='lower right',
               ncol=3,
               fontsize=7)
    plt.title('Iris ' + feature + ' Feature')
    plt.xlabel(feature + ' length')
    plt.ylabel(feature + ' width')
    plt.show()


def make_prediction(x_train, y_train, x_test, k):
    # Square for i-vector on train with all vectors in test
    # Get Euclidean distance between i-vector and test data
    # Store on distances list
    # Sort list of distances
    distances = sorted([[np.sqrt(np.sum(np.square(x_test - x_train[i, :]))), i] for i in range(len(x_train))])

    # Get name of K neighbors (spices) targets with less distance
    targets = [y_train[distances[i][1]] for i in range(k)]

    # Get most common observation
    best = Counter(targets).most_common(1).pop()
    print("Prediction for ", x_test, "is >>", best[0])
    return best[0]


def knn(x_train, y_train, x_test, k):
    # For each test observation make prediction for each x_test vector
    return [make_prediction(x_train, y_train, x_test[i, :], k) for i in range(len(x_test))]


def main():
    k = 10
    test_size = 0.33
    data = pd.read_csv('dataset.csv')
    print("**********Initial parameters**********")
    print("K:", k)
    print("Test sizes:", test_size * 100, "%")
    plot = input("Plot data? [y/N]>>")
    if plot == 'y' or plot == 'Y':
        plot_data(data['sepal_length'], data['sepal_width'], "Sepal")
        plot_data(data['petal_length'], data['petal_width'], "Petal")
    X = np.array(data.iloc[:, 0:4])  # Getting input matrix
    y = np.array(data.iloc[:, 4])  # Getting target vector
    # Split data as train data and test data
    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=42)
    print("**********KNN Start**********")
    print("\t\t\t\t  s_l|s_w|p_l|p_w")
    predictions = knn(x_train, y_train, x_test, k)  # Run KNN algorithm to get predictions
    predictions = np.asarray(predictions)  # Convert to np array for accuracy function
    accuracy = accuracy_score(y_test, predictions)  # Get accuracy
    print("**********Real vs Predictions**********")
    for i in range(len(y_test)):
        if predictions[i] == y_test[i]:
            print(predictions[i], '==', y_test[i])
        else:
            print(predictions[i], '<<>>', y_test[i], '(X)')
    print("*"*35)
    print("Accuracy >>", accuracy*100, "%")


if __name__ == '__main__':
    main()
