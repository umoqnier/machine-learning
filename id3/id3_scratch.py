from math import log
from sys import argv
import pandas as pd

key_feature = "descuento"
features = ["ejemplares", "ventas", "precio"]

class Node:
    def __init__(self, string="", children=[]):
        self.string = string
        self.children = children


def get_weighteds(feature):
    weighteds = dict()
    data = pd.read_csv('books_dataset.csv', sep=',')
    for element, key_f in zip(data[feature], data[key_feature]):
        if element not in weighteds.keys():
            weighteds[element] = [1, 0]
        else:
            weighteds[element][0] += 1

        if int(key_f):
            weighteds[element][1] += 1

    return weighteds


def entropy(positive, negative):
    total = positive + negative
    e = -(positive/total)*log(positive/total, 2) - (negative/total)*log(negative/total, 2)
    return e


def gain(feature):  # todo"""docstring for Node."""
    gain = 0
    weighteds = get_weighteds(feature)
    for weighted in weighteds.values():
        pass


def check_all_positive(data):
    pass


def check_all_negative(data):
    pass


def id3(data, objetivo, atributos):
    if check_all_positive(data):
        return Node('+')
    elif check_all_negative(data):
        return Node('-')
    elif not atributos:
        return Node(most_common(data[key_feature]))
    else:


def get_best_attribute():
    pass


def main():
    aux = 0
    negatives = len(filter(lambda x: x == 0, data[key_feature]))
    positives = len(data[key_feature]) - negatives
    entropia_inicial = entropy(positives, negatives)
    best_attribute = get_best_attribute(entropia_inicial, features)
    tree = id3(data, best_attribute, features)
    


if __name__ == '__main__':
    main()
