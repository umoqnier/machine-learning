from math import log
from sys import argv


def get_entropy(positive, negative):
    total = positive + negative
    e = -(positive/total)*log(positive/total, 2) - (negative/total)*log(negative/total, 2)
    return e


def get_gain(**kargs):  # todo
    args = list(args)
    total_ocurrences = int(args.pop())
    gain = 0
    for arg in args:
        gain += -(arg/total_ocurrences) * get_entropy()
    # g = 1 - (p/t)*get_entropy()


"""def main():
    op = input()
    if op == "e":
        print(get_entropy(int(argv[1]), int(argv[2])))
    elif op == "g":
        args = {"ocurrence": argv[1], "positive": argv[2], "negative": argv[3]]}
        get_gain(1, 2, 3, 4)"""

