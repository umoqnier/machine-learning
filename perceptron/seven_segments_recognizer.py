import random
import numpy as np
from functools import reduce


def hardlim(vector):
    result = []
    for num in vector:
        if num >= 0:
            result.append(1)
        else:
            result.append(0)
    return np.array(result)


            #  a  b  c  d  e  f  g
p = np.array([[1, 1, 1, 1, 1, 1, 0],  # 0
              [0, 1, 1, 0, 0, 0, 0],  # 1
              [1, 1, 0, 1, 1, 0, 1],  # 2
              [1, 1, 1, 1, 0, 0, 1]])  # 3

t = [np.array([0] * len(p[0])), np.array([0] * (len(p[0]) - 1) + [1]),
     np.array([0] * (len(p[0]) - 2) + [1, 0]), np.array([0] * (len(p[0]) - 2) + [1, 1])]
w = np.array([random.uniform(-0.5, 0.5) for _ in range(len(p[0]))])
b = random.uniform(-1, 1)
band = 0

for epoch in range(0, 100000):
    for i, sample in enumerate(p):
        n = w * p[i] + b
        a = hardlim(n)
        error = t[i] - a
        w = w + error * p[i]
        b = b + error
    if not (1 in [abs(e_) for e_ in error]):
        band = 1
        break
print("termino en el epoch", epoch)

# Testing
with open("test_7sr.txt", 'r') as f:
    errors = {}
    data = f.read()
    bits = np.array([int(i) for i in data.split(',')])
    for i, sample in enumerate(p):
        n = w * bits + b
        a = hardlim(n)
        error = t[i] - a
        errors[str(i)] = reduce(lambda x, y: abs(x) + abs(y), error)

    print("El numero leido es el: ", min(errors, key=errors.get))

