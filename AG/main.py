import numpy as np
import random
import pandas as pd
import itertools
import matplotlib.pyplot as plt

__author__ = "Diego A. Barriga Martínez"


def plot_twist(data, c_rate, m_rate):
    plt.plot([_ for _ in range(len(data))], data, 'g', linewidth=2)
    plt.xticks([_ for _ in range(len(data))])
    plt.xlabel("Generaciones")
    plt.ylabel("Ganancia")
    plt.title("Evolución de 20 generaciones. Cross rate: " + str(c_rate * 100) + " Mutation rate: " + str(m_rate * 100))
    plt.grid()
    plt.show()


def mutation(mutation_rate, population_quantity, variables, population):
    total_generation = variables * population_quantity
    mutation_gen_number = round(mutation_rate * total_generation)
    selected_guys = [random.randint(0, len(population) - 1) for _ in range(int(mutation_gen_number / 2))]
    selected_gen = [random.randint(0, len(population[0]) - 1) for _ in range(int(mutation_gen_number / 2))]
    selected_indexes = [(s_guy, s_gen) for s_guy, s_gen in zip(selected_guys, selected_gen)]
    for i in range(0, len(selected_indexes) - 1, 2):
        aux_1 = population[selected_indexes[i][0]][selected_indexes[i][1]]
        aux_2 = population[selected_indexes[i + 1][0]][selected_indexes[i + 1][1]]
        population[selected_indexes[i][0]][selected_indexes[i][1]] = aux_2
        population[selected_indexes[i + 1][0]][selected_indexes[i + 1][1]] = aux_1


def crossover(combinations, population, data):
    aux_population = population[:]
    aux_1, aux_2 = list(), list()
    for i, combination in enumerate(combinations):
        guy_1 = population[combination[0]]
        guy_2 = population[combination[1]]
        b_guy_1 = [np.binary_repr(n, width=4) for n in guy_1]
        b_guy_2 = [np.binary_repr(n, width=4) for n in guy_2]
        b_guy_1 = list(''.join(b_guy_1))
        b_guy_2 = list(''.join(b_guy_2))
        size = min(len(b_guy_1), len(b_guy_2))
        cross_point_1 = random.randint(1, size)
        cross_point_2 = random.randint(1, size - 1)  # TODO: two cross points
        if cross_point_2 >= cross_point_1:
            cross_point_2 += 1
        else:  # Swap the two cross points
            cross_point_1, cross_point_2 = cross_point_2, cross_point_1
        b_guy_1[cross_point_1:cross_point_2], b_guy_2[cross_point_1:cross_point_2] = \
            b_guy_2[cross_point_1:cross_point_2], b_guy_1[cross_point_1:cross_point_2]
        for j in range(0, 16, 4):
            aux_1.append(int("".join(b_guy_1[j:j+4]), base=2))
            aux_2.append(int("".join(b_guy_2[j:j+4]), base=2))
        guy_1 = np.array(aux_1)
        guy_2 = np.array(aux_2)
        winner_i = combination[0] if aptitude(guy_1, data) > aptitude(guy_2, data) else combination[1]
        aux_population[winner_i] = guy_1 if aptitude(guy_1, data) > aptitude(guy_2, data) else guy_2
        aux_1, aux_2 = [], []
    return aux_population


def unreal_tournament(evaluation_set, contenders_number=2):
    winners = []
    rounds = len(evaluation_set) / (len(evaluation_set) / contenders_number)
    duels = len(evaluation_set) / rounds
    while rounds:
        pairs = random.sample(range(50), 50)
        np.random.shuffle(evaluation_set)
        j = 0
        while duels:
            fighter_1 = evaluation_set[pairs[j]]
            fighter_2 = evaluation_set[pairs[j+1]]
            if fighter_1 > fighter_2:
                winners.append(pairs[j])
            else:
                winners.append(pairs[j+1])
            j += 2
            duels -= 1
        rounds -= 1
    return winners


def aptitude(guy, data_set):
    gain = 0.0
    V = abs(np.sum(guy) - 10)
    for i, money in enumerate(guy):
        if money > 10:
            money = 10  # To avoid indexer is out-of-bounds exception result of crossover function
        gain += data_set.iloc[money, i]
    return gain / ((500 * V) + 1)


def generate_population(population_quantity, var_quantity):
    return np.random.random_integers(0, 9, (population_quantity, var_quantity))


def main():
    data = pd.read_csv('data.csv')
    population_quantity = 50
    variables_quantity = len(data.iloc[0, :])
    generations = 20
    cross_rates = [0.8, 0.5, 0.75]
    mutation_rates = [0.1, 0.3, 0.15]
    for (cross_rate, mutation_rate) in zip(cross_rates, mutation_rates):
        population = generate_population(population_quantity, variables_quantity)
        to_plot = [max([aptitude(guy, data) for guy in population])]  # Initial Gain
        print('*'*25)
        print("Cross rate:", cross_rate)
        print("Mutation rate:", mutation_rate)
        print("Gananica inicial", to_plot[0])
        print('*' * 25)
        while generations:
            evaluation_set = [aptitude(guy, data) for guy in population]  # Evaluation of each guy
            bests_index = unreal_tournament(evaluation_set)  # Selection by tournament
            rs = np.random.random_sample(50)
            guys_to_cross = [i for i, r in zip(bests_index, rs) if r < cross_rate]  # Selecting number of guys to cross
            population = crossover(list(itertools.combinations(guys_to_cross, 2)), population, data)  # Update population
            mutation(mutation_rate, population_quantity, variables_quantity, population)
            current_gain = 0
            for i, guy in enumerate(population):
                new_gain = aptitude(guy, data)
                if current_gain < new_gain:
                    current_gain = new_gain
                    best_index = i
            to_plot.append(current_gain)
            print("*********\nGeneración:", abs(generations - 20), "\nIndividuo:", population[best_index],
                  "\nGanancia:", current_gain)
            generations -= 1
        plot_twist(to_plot, cross_rate, mutation_rate)
        generations = 20


if __name__ == '__main__':
    main()
