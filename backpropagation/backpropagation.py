#!/usr/bin/python3.6
import numpy as np
import random
import matplotlib.pyplot as plt
from math import e, sin, pi


def logsim(vector):
    return np.array([(1 / (1 + e**(-n))) for n in vector])


def target_1(p):
    return 1 + sin((pi/4) * p)


def target_2(p):
    return 1 + sin((pi / 2) * p)


def backpropagation(w_1, b_1, w_2, b_2, alfa, delta, iterations, target):
    for i in range(0, iterations):
        # Get random number between -2 and 2
        p = random.uniform(-2, 2)
        a_0 = p

        # First layer
        n = w_1 * a_0 + b_1
        a_1 = logsim(n)

        # Second layer
        a_2 = w_2.dot(a_1).T + b_2

        # Calculate error
        error = target(p) - a_2

        # Derivatives
        diagonal = np.array([((1 - element) * element) for element in a_1])
        d_1 = np.zeros((len(diagonal), len(diagonal)))
        np.fill_diagonal(d_1, diagonal)
        d_1 = np.matrix(d_1)  # Derivative of log sigmoid (1 - a1) * a1
        d_2 = 1

        # Backpropagation
        # Sensibilities
        s_2 = -2 * d_2 * error
        s_1 = d_1 * np.matrix(w_2).T * s_2
        s_1 = np.squeeze(np.asarray(s_1))

        # Update weights
        w_2 = w_2 - alfa * s_2 * a_1
        b_2 = b_2 - alfa * s_2

        w_1 = w_1 - alfa * s_1 * a_0
        b_1 = b_1 - alfa * s_1

        # Graphics
        if i < 2:
            x = np.linspace(-2, 2, 100)
            y = [target(p) for p in np.linspace(-2, 2, 100)]

            x_i = p
            y_i = target(p)
            x_l = p
            y_l = a_2

            fig, ax = plt.subplots()
            ax.set(title='Iter #' + str(i) + ' t: %.2f || a_2: %.2f || e: %.5f' % (target(p), a_2, error), ylabel='g(p)')
            ax.plot(x, y)
            ax.annotate('p(%.2f, %.2f)' % (x_i, y_i), xy=(x_i, y_i), xytext=(x_i, y_i - y_i*0.2),
                        arrowprops=dict(facecolor='black', shrink=0.01))
            ax.annotate('a_2(%.2f, %.2f)' % (x_l, y_l), xy=(x_l, y_l), xytext=(x_l, y_l + y_l*0.2),
                        arrowprops=dict(facecolor='blue', shrink=0.01))
            plt.show()
            print('target: %.2f - a_2: %.2f - error: %.5f' % (target(p), a_2, error))
        if abs(error) <= delta or i == iterations - 1:
            x = np.linspace(-2, 2, 100)
            y = [target(p) for p in np.linspace(-2, 2, 100)]

            x_i = p
            y_i = target(p)
            x_l = p
            y_l = a_2

            fig, ax = plt.subplots()
            ax.set(title='Final Iter #' + str(i) + ' t: %.2f || a_2: %.2f || e: %.5f' % (target(p), a_2, error),
                   ylabel='g(p)')
            ax.plot(x, y)
            ax.annotate('p(%.2f, %.2f)' % (x_i, y_i), xy=(x_i, y_i), xytext=(x_i, y_i - y_i * 0.2),
                        arrowprops=dict(facecolor='black', shrink=0.01))
            ax.annotate('a_2(%.2f, %.2f)' % (x_l, y_l), xy=(x_l, y_l), xytext=(x_l, y_l + y_l * 0.2),
                        arrowprops=dict(facecolor='blue', shrink=0.01))
            plt.show()

            print('target: %.2f - a_2: %.2f - error: %.5f' % (target(p), a_2, error))
            return w_1, w_2, b_1, b_2


def main():
    exercise = input("1. Exercise 1  2. Exercise 2 >>[1|2] ")
    if exercise == '1':
        # Initial data
        w_1 = np.array([-0.27, -0.41])
        b_1 = np.array([-0.48, -0.13])
        w_2 = np.array([0.09, -0.17])
        b_2 = 0.48
        alfa = 0.10  # learning rate
        delta = 0.00001  # To converge
        iterations = 10000
        w_1, w_2, b_1, b_2 = backpropagation(w_1, w_2, b_1, b_2, alfa, delta, iterations, target_1)
        print("Weight 1:", w_1, "Weight 2:", w_2, "Bias 1:", b_1, "Bias 2:", b_2)
    elif exercise == '2':
        sub = input("Subsection?>>[a|b|c] ")
        # Initial data
        w_1 = np.array([random.uniform(-0.5, 0.5), random.uniform(-0.5, 0.5)])
        b_1 = np.array([random.uniform(-0.5, 0.5), random.uniform(-0.5, 0.5)])
        w_2 = np.array([random.uniform(-0.5, 0.5), random.uniform(-0.5, 0.5)])
        b_2 = random.uniform(-0.5, 0.5)
        delta = 0.00001  # To converge
        iterations = 10000
        if sub == "a":
            alfa = 0.5  # learning rate
        elif sub == "b":
            alfa = 1.0  # learning rate
        elif sub == "c":
            w_1 = np.array([random.uniform(-0.5, 0.5) for _ in range(10)])
            b_1 = np.array([random.uniform(-0.5, 0.5) for _ in range(10)])
            w_2 = np.array([random.uniform(-0.5, 0.5) for _ in range(10)])
            b_2 = random.uniform(-0.5, 0.5)
            alfa = 0.5  # learning rate
        w_1, w_2, b_1, b_2 = backpropagation(w_1, w_2, b_1, b_2, alfa, delta, iterations, target_2)
        print("Weight 1:", w_1, "Weight 2:", w_2, "Bias 1:", b_1, "Bias 2:", b_2)
    else:
        print("Error: Incorrect input. Select one valid option")


if __name__ == '__main__':
    main()
