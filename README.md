# Machine-learning
Todos los programas de este repositorio requieren de `python3.x` y fueron probados en GNU/Linux

## Bibliotecas externas
* numpy instalar con `pip install numpy`
* matplotlib instalar con `pip intall matplotlib`
* pandas instalar con `pip install pandas`

## K-Nearest-Neighbors
Clasificador no lineal encargado de seleccionar una especie de Iris con base en el conocido [dataset](https://archive.ics.uci.edu/ml/datasets/Iris) de Ronald Fisher.
### Uso del programa
Colocarse en la carpeta `knn` y correr el programa con `python iris_clasiffier.py`.

## Algoritmos Genéticos
Programa encargado de verificar cual es la inversión optima para cada una de cuatro tiendas con base en una función de aptitud dada.

### Uso del programa
* Entrar a la carpeta AG
* Correr el programa con el comando `python main.py`
* El programa correrá con 3 diferentes valores de *cross rate* y *mutation rate*
* Se imprimen en terminal los datos iniciales que son *cross rate*, *mutation rate* y la *ganancia inicial*
* Se imprime cada generación con el individuo que maximiza la ganancia y la ganancia en turno
* Al terminar mostrará la gráfica de evolución. **NOTA: Para continuar con los siguientes valores de *cross rate* y *mutation rate* se debe cerrar la gráfica en turno** 

## Backpropagation
### Bibliotecas externas
* numpy instalar con `pip install numpy`
* matplotlib instalar con `pip intall matplotlib`

### Uso del programa
* Al iniciar el programa pedirá una entrada 1 o 2 correspondiente a cada ejercicio
* En caso de elegir 2 pedira una segunda entrada correspondiente a cada inciso a, b o c
* En cualquier caso apareceran tres graficas correpondientes a la iteración 1, 2 y la de convergencia o en su caso la última iteracion permitida que esta definida como 10000


*Algoritmos del curso de aprendizaje de la FI UNAM 2019-1*
